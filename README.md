# Mist Blue (GeekTool Theme)

## Attribution:

- this theme was inspired by
  [Flat Blue Theme](https://www.deviantart.com/dd-921/art/Flat-Blue-Theme-1-2f-594102810)
  for rainmeter on Windows OS
- the original artwork
  ([Lakeside Sunrise](https://dribbble.com/shots/1816328-Lakeside-Sunrise)) was
  created by [Louis Coyle](https://dribbble.com/louiscoyle), fan art inspired by
  Firewatch
- the animation ([The 25th Hour](http://louie.co.nz/25th_hour/)) was created by
  [Louis Coyle](https://dribbble.com/louiscoyle) as supplementary to
  [Lakeside Sunrise](https://dribbble.com/shots/1816328-Lakeside-Sunrise)

## Dependencies:

- macOS
- [GeekTool](https://www.tynsoe.org/v2/geektool/)

## Setup

1. install [Geektool](https://www.tynsoe.org/v2/geektool/)

2. add font to fontbook

3. set [foreground](/Script/foreground.md): open `foreground.glet` in GeekTool

4. set [date](/Script/date.md): open `date.glet` in GeekTool

5. set [clock](/Script/clock.md): open `clock.glet` in GeekTool

### Note:

1. adjust the size and position to fit your screen resolution
2. included is [The 25th Hour](/Animation/The_25th_Hour.htm) for those with
   Quartz Composer and NerdTool installed. NerdTool does not work on macOS
   Mojave so I am currently unable to add animation.
